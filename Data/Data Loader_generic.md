# Data Loader
## Document info
* Software : Pokémon SDK, known as PSDK
* Author : Nuri Yuri
* Reviewer : *None*
* Specification type : Generic

## Specification Info

* Feature name : Data Loader
* Description :  
	Module that helps PSDK to load data from text & binary source according to the project state.  
	   
	PSDK is on the road of "full git compatibility", to acheive that, data needs to be stored in various text sources (tmx, tsx, ruby, yaml, csv, etc...) but the loading times in Release should be short. Text format is unfortunately the slowest way of loading data, that's why we need a module that reduce the loading times and help to use text data.

### Loading data

The Data Loader will know two state : Release and non-Release.

In Release state, the Data Loader will directly load the binary data using load_data when asked to.

In non-Release state, the Data Loader will check the text file corresponding to the wanted data and load it according to the following condition :
* The binary data file doesn't exists.
* The `mtime` of the text file is higher than the `mtime` of the binary file.

If a text file is loaded by the Data Loader, the resulting object will be stored in the corresponding binary file. The Data Loader should save the resulting object once the game window closed and no error happened during the game processing.

If there's no text file correspondance (RMXP maps) the Data Loader will load the binary file without checking the text file.

### Association between text file & binary files

To know what to load the Data Loader will require the following information :
* Type of data (symbol, like `:item`)
* Text file format (string, like `'Data/items/yml/%<item_id>03d.yml'`)
* Binary file format (string, like `'Data/items/%<item_id>03d.dat'`)
* The list of format hash key (in this case `:item_id`)

The text file format can be `nil` to explictely say there's no text file for the binary file. The maker has to ensure the binary file always exists if no text file associated to the binary file.

The type of data will help the user to request the data.

### Request data

The Data Loader will have to be the module used for all data request (by the GameData interfaces or by the in game processes). In the end we should not see any `$game_data_xxx` or `$data_xxx` global other than `$data_system`.

To request data, the user will ask the Data Loader (through the right function) a specific data from a specify type of data. The other parameters of the function will be the hash key => value helping to format the filenames. And example of parameter would be `(:item, item_id: 30)`.

### Request a collection

To fetch data transparently (without having to precise all the parameter) the Data Loader will expose a collection request function that will return an "Array" like interface allowing the user to fetch the data using only the Type of Data as parameter. The `[]` and the `dig` function will be charged to call the Request Data function with the right parameter if the data hasn't been loaded.

The Array like interface will be living in the Data Loader once the text -> binary association has been defined. That will be some kind of cache.