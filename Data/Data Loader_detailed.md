# Data Loader
## Document info
* Software : Pokémon SDK, known as PSDK
* Author : Nuri Yuri
* Reviewer : *None*
* Specification type : Detailed

## Specification Info
* see [Data Loader_generic](./Data Loader_generic.md)

The Data Load will be a class that will provide **through its instances** an access to the loading data function, the collection interfaces and the text -> binary associations.

The Data Loader will provide access to the request data, request a collection and associate text to binary functions using **class methods**.

A `CollectionLink` class will be defined in the Data Loader, the `CollectionLink` class aim to give access to the collection data, provide the `[]` and `dig` functions, store the text -> binary information and implement the text data loading & data saving functions. The `CollectionLink` should be inherited to define the right text data loading function. This implies that the Association between text file & binary file needs a `CollectionLink` child class as parameter.

### Loading data

The `CollectionLink` will be in charge of loading the data, the class will expose the `load` function and this function will perform the following :

> Translate the filenames using the parameters and the translation info
>
> If the game is not in release and there's a text file
>   > If the binary file doesn't exists or the text file mtime > binary file mtime
>   >   > Invoke the load text function  
>   >   > Store the parameter in the to_store info  
>   >   > `Return` the result.
>
> Load the binary file.

If one of the file doesn't exists, the function has to log the error and return the default object to let the game run.

#### Store data in binary files

The Data Loader will have a store data property set to true by default if the game is not in release. This property will be set to false if `Yuki::EXC` or if `Yuki::... handle_error` processed an error that isn't Reset or Closing the Window.

The Data Loader will register a `END` process that will check the store data property and call the `store_data_in_binary_files` function from the Data Loader.

The `store_data_in_binary_files` will iterate throught all the `CollectionLink` and call their `store_data_in_binary_files` function. The function will save the data marked by to_store info.

### Association between text file & binary files

As said in the Generic specifications, we need to tell the Data Loader the information about loading a specific kind of data : it's kind, the location of text files & binary files and also the parameters.

The Data Loader will create a `CollectionLink` link from that and store it in a data collections hash that use the kind of data as a key.

If a association is made on a already existing association, it'll be overwritten. 

For dynamism in data loading we'll need to tell the kind of `CollectionLink` we need because telling the text files location is not enough, we need to know how to parse the text file.

Here's the method parameters : `create_collection(data_type, binary_file_format, text_file_format, collection_type, *parameters)`
* `data_type` will be a symbol
* `binary_file_format` will be a string
* `text_file_format` will be a string or nil
* `collection_type` will be a class
* `parameters` will be an array telling how to format the filenames.

### Request data

This function will perform the following thing according to the `data_type` (symbol) and `parameters` (hash, nil) info :

> Fetch the `CollectionLink`
>
> If the `CollectionLink` doesn't exists
>   > raise a LoadError exception telling "Data Loader for #{data_type} has not been defined!"
>
> If the `parameters` is nil
>   > `Return` the `CollectionLink` data property (The `CollectionLink` will load the data if it's nil.)
> 
> Construct the `dig` parameters according to the `CollectionLink` parameters : `dig_args = collection.parameters.collect { |key| parameters[key] }`  
> `Return` the `CollectionLink` dig result using the `dig_args`

### Request a collection

This function will return the `CollectionLink` of a specific data type. Here's what it should do :

> Fetch the `CollectionLink`
>
> If the `CollectionLink` doesn't exists
>   > raise a LoadError exception telling "Data Loader for #{data_type} has not been defined!"
>
> `Return` the `CollectionLink`